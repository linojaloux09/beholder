#include <Adafruit_AHTX0.h>

Adafruit_AHTX0 aht;

void init_capteur()
{
  if (!aht.begin()) {
    Serial.println("erreur capteur");
    while (1) 
      delay(10);
  }
}

float readTemperature() 
{
  sensors_event_t temp;
  aht.getEvent(NULL, &temp);
  return temp.temperature;
}

float readHumidity() 
{
  sensors_event_t humidity;
  aht.getEvent(&humidity, NULL);
  return humidity.relative_humidity;
}

int readCO2() 
{
  return analogRead(A0);
}

void setup() {
  // Démarrer la communication série avec le moniteur série
  Serial.begin(9600); // Début de la communication série à une vitesse de 9600 bauds
  
  // Initialisation du capteur d'humidité AHT20
  Serial.println("Adafruit AHT10/AHT20 demo!");
  init_capteur();
  Serial.println("capteur OK");
}

void loop() {
  // Appeler les trois fonctions pour obtenir les données et les afficher
  float temperature = readTemperature();
  float humidity = readHumidity();
  int co2Value = readCO2();

  Serial.println("Temperature: "+String(temperature)+" degrees C");
  Serial.println("Humidity: "+String(humidity)+"%");
  Serial.println("C02: "+String(co2Value)+" ppm");
 
  // Attendre un certain temps avant de lire à nouveau les valeurs
  delay(1000); // Attendre 1 seconde avant de lire à nouveau les valeurs
}
