import csv
import time

# Chemin du fichier CSV à créer ou à écrire
csv_file = 'sensor_data.csv'

while True:
    # Exemple de données extraites des capteurs (à remplacer par vos propres données)
    sensor_data = [
        {'temperature': 25.5, 'humidity': 60.0, 'co2': 500.0},
        {'temperature': 26.0, 'humidity': 59.5, 'co2': 510.0},
        {'temperature': 25.8, 'humidity': 59.8, 'co2': 520.0}
    ]

    # Écriture des données dans le fichier CSV
    with open(csv_file, mode='w', newline='') as file:
        writer = csv.writer(file)

        # Écriture de l'en-tête du fichier CSV
        writer.writerow(['Temperature', 'Humidity', 'CO2'])

        # Écriture des données extraites dans le fichier CSV
        for data_point in sensor_data:
            writer.writerow([data_point['temperature'], data_point['humidity'], data_point['co2']])

    # Attendre avant de lire les nouvelles données (par exemple, toutes les 5 minutes)
    time.sleep(300)

