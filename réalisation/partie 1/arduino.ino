unsigned int i=0;

void setup(void) 
{
  Serial.begin(9600);
  Serial.println("Un message va etre envoye toutes les deux secondes des maintenant !");
}

void loop(void) 
{
  String message = "Message #" + String(i);
  Serial.println(message);
  delay(2000);
  i++;
}
