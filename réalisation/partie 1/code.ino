void setup() {
  // Démarrer la communication série avec le moniteur série
  Serial.begin(9600); // Début de la communication série à une vitesse de 9600 bauds
}

void loop() {
  // Lire la valeur de CO2 du capteur de qualité de l'air
  int co2Value = analogRead(A0); // Lecture de la valeur analogique sur la broche A0

  // Afficher la valeur de CO2 sur le moniteur série
  Serial.print("CO2 (ppm): "); // Affichage du texte "CO2 (ppm): "
  Serial.println(co2Value); // Affichage de la valeur de CO2 suivie d'un retour à la ligne

  // Attendre un certain temps avant de lire à nouveau la valeur
  delay(500); // Attendre 0.5 seconde (500 millisecondes) avant de lire à nouveau la valeur
}

