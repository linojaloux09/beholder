import serial

# Ouverture de la connexion série avec l'Arduino
Arduino = serial.Serial('/dev/ttyACM0', 9600)

# Partie 1 : Lecture des données brutes de l'Arduino
while True:
    # Lecture d'une ligne de données brutes depuis l'Arduino et décodage en UTF-8
    raw_data = Arduino.readline().decode("utf-8").rstrip()
    
    # Affichage des données brutes
    print(raw_data)

# Partie 2 : Extraction des valeurs de capteurs à partir des données brutes
# Exemple de données brutes reçues depuis l'Arduino (à remplacer par vos données réelles)
data_from_arduino = "temperature:25.5,humidity:60,co2:500"

# Fonction pour extraire les valeurs de température, d'humidité et de CO2 à partir des données brutes
def extract_sensor_values(data):
    # Initialisation d'un dictionnaire pour stocker les valeurs des capteurs
    sensor_values = {}
    
    # Parcourir les éléments de la chaîne de données brutes séparés par des virgules
    for item in data.split(','):
        # Séparer la clé et la valeur de chaque élément à l'aide de la méthode split() avec ':' comme délimiteur
        key, value = item.split(':')
        # Convertir la valeur en flottant et stocker dans le dictionnaire sensor_values
        sensor_values[key] = float(value)  # Convertir les valeurs en flottants si nécessaire
    
    # Retourner le dictionnaire des valeurs des capteurs
    return sensor_values

# Extraction des valeurs
sensor_values = extract_sensor_values(data_from_arduino)

# Affichage des valeurs extraites
print("Température:", sensor_values['temperature'])
print("Humidité:", sensor_values['humidity'])
print("CO2:", sensor_values['co2'])
