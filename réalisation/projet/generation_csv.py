import csv  # Module pour la manipulation de fichiers CSV
import random  # Module pour la génération de nombres aléatoires
import time  # Module pour la gestion du temps
from datetime import datetime  # Module pour la manipulation des dates et heures

# Fonction pour générer des données aléatoires
def generate_random_data():
    temperature = round(random.uniform(18, 30), 2)
    humidity = round(random.uniform(30, 70), 2)
    co2 = round(random.uniform(300, 1000), 2)
    return temperature, humidity, co2

# Fonction pour fermer le fichier CSV en cours, en créer un nouveau avec un nom basé sur la date actuelle, et réinitialiser l'entête
def create_new_csv_file():
    global csv_file, writer, csv_filename
    csv_file.close()
    current_datetime = datetime.now()
    csv_filename = current_datetime.strftime('%Y-%m-%d_%H-%M-%S') + '_capteur_data.csv'
    csv_file = open(csv_filename, mode='w', newline='')
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()

# Obtention du nom du fichier CSV initial basé sur la date et l'heure actuelles
current_datetime = datetime.now()
csv_filename = current_datetime.strftime('%Y-%m-%d_%H-%M-%S') + '_capteur_data.csv'

# Ouverture du fichier CSV en mode écriture
csv_file = open(csv_filename, mode='w', newline='')
fieldnames = ['Timestamp', 'Temperature (°C)', 'Humidity (%)', 'CO2 (ppm)']
writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

# Écriture de l'en-tête du fichier CSV
writer.writeheader()

try:
    while True:
        current_datetime = datetime.now()
        # Vérification si l'heure actuelle est minuit
        if current_datetime.hour == 0 and current_datetime.minute == 0:
            create_new_csv_file()  # Création d'un nouveau fichier CSV
            
        temperature, humidity, co2 = generate_random_data()
        timestamp = time.strftime('%Y-%m-%d %H:%M:%S')
        
        # Écriture des données dans le fichier CSV
        writer.writerow({'Timestamp': timestamp,
                         'Temperature (°C)': temperature,
                         'Humidity (%)': humidity,
                         'CO2 (ppm)': co2})
        
        print(f'Données enregistrées : {timestamp}, {temperature}°C, {humidity}%, {co2}ppm')
        time.sleep(5)  # Attente de 5 secondes avant la prochaine génération de données
except KeyboardInterrupt:
    print("Arrêt de la génération de données.")
finally:
    csv_file.close()  # Fermeture du fichier CSV à la fin de l'exécution du programme
