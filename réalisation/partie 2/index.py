from flask import Flask, render_template, jsonify
import rasp

app = Flask(__name__)

# Ouvrir le port série au démarrage de l'application Flask
Arduino = rasp.ouvrir_port('/dev/ttyACM0', 9600)

@app.route('/')
def index():
    return render_template("index.html")

# Route pour récupérer les données du capteur au format JSON
@app.route('/data')
def get_data():
    data = rasp.lecture(Arduino)
    temp, CO2, hum = data
    return jsonify(temperature=temp, CO2=CO2, humidity=hum)

if __name__ == "__main__":
    app.run(debug=True)

# Fermer le port série à l'arrêt de l'application Flask
@app.before_request
def before_request_func():
    pass

@app.teardown_appcontext
def teardown_appcontext_func(error=None):
    if Arduino.is_open:
        Arduino.close()
