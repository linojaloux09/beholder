import serial

def ouvrir_port(port, vitesse):
    return serial.Serial(port, vitesse)

def fermeture_port(port_serie):
    return port_serie.close()


def lecture(port_serie):
    data = port_serie.readline().decode("utf-8").rstrip()
    # Séparer les données en utilisant les libellés comme séparateurs
    temp_start = data.find('Temperature:') + len('Temperature:')
    temp_end = data.find('degrees C')
    temp = float(data[temp_start:temp_end])

    hum_start = data.find('Humidity:') + len('Humidity:')
    hum_end = data.find('%')
    hum = float(data[hum_start:hum_end])

    co2_start = data.find('CO2:') + len('CO2:')
    co2 = float(data[co2_start:])

    return temp, hum, co2

if __name__ == "__main__":
    Arduino = ouvrir_port('/dev/rfcomm0', 9600)  # Assurez-vous que le port est correct
    try:
        while True:
            print(lecture(Arduino))
    except KeyboardInterrupt:
        Arduino.close()
        print("Fermeture du port série.")
