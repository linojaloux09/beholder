import csv  # Importer le module csv pour la manipulation des fichiers CSV
import datetime  # Importer le module datetime pour obtenir la date et l'heure actuelles

def enregistrer_donnees_csv(data):
    # Nom du fichier CSV
    fichier_csv = 'donnees_capteur.csv'
    
    # Créer ou ouvrir le fichier CSV en mode append
    with open(fichier_csv, 'a', newline='') as file:
        writer = csv.writer(file)  # Créer un objet writer pour écrire dans le fichier CSV
        
        # Si le fichier est vide, écrire l'en-tête
        if file.tell() == 0:
            writer.writerow(['Date', 'Température', 'Humidité', 'CO2'])  # Écrire l'en-tête
        
        # Récupérer la date actuelle au format YYYY-MM-DD HH:MM:SS
        date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        
        # Ajouter les données au fichier CSV
        writer.writerow([date, *data])  # Écrire la date, la température, l'humidité et le CO2 dans une ligne

# Exemple d'utilisation
data = (21.85, 34, 85.01)  # Exemple de données
enregistrer_donnees_csv(data)  # Appel de la fonction pour enregistrer les données dans le fichier CSV
