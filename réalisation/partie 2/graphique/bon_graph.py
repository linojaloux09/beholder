import matplotlib.pyplot as plt
import csv

# Chemin du fichier CSV contenant les données
csv_file = 'donnees_capteur.csv'  # Remplacez 'donnees_capteur.csv' par le nom de votre fichier CSV

# Listes pour stocker les données
dates = []
co2_values = []

# Lecture des données à partir du fichier CSV
with open(csv_file, 'r') as file:
    reader = csv.reader(file)
    next(reader)  # Ignore l'en-tête du fichier CSV
    for row in reader:
        date = row[0]  # Première colonne : date
        co2 = float(row[3])  # Quatrième colonne : valeur de CO2 (convertie en flottant)
        dates.append(date)
        co2_values.append(co2)

# Création du graphique
plt.figure(figsize=(10, 6))
plt.plot(dates, co2_values, color='blue', marker='o', linestyle='-')
plt.title('Évolution des niveaux de CO2')
plt.xlabel('Date')
plt.ylabel('Niveau de CO2 (ppm)')
plt.xticks(rotation=45)  # Rotation des dates pour une meilleure lisibilité
plt.tight_layout()

# Affichage du graphique
plt.show()
