from flask import Flask, render_template, jsonify
import rasp
import csv
import matplotlib.pyplot as plt
from datetime import datetime
import os

app = Flask(__name__)

# Ouvrir le port série au démarrage de l'application Flask
Arduino = rasp.ouvrir_port('/dev/rfcomm0', 9600)

# Fonction pour lire les données à partir du fichier CSV
def read_csv(filename):
    dates = []
    temperatures = []
    humidity = []
    co2 = []

    with open(filename, 'r') as file:
        reader = csv.reader(file)
        next(reader)  # Skip header row
        for row in reader:
            try:
                date = datetime.strptime(row[0], '%Y-%m-%d %H:%M:%S')
                temperature = float(row[1])
                humidity_value = float(row[2])
                co2_value = float(row[3])   
                    
                dates.append(date)
                temperatures.append(temperature)
             
                humidity.append(humidity_value)
                co2.append(co2_value)
            except (ValueError, IndexError):
                pass

    return dates, temperatures, humidity, co2

# Route pour afficher la page d'accueil
@app.route('/')
def index():
    return render_template("index.html")

# Route pour récupérer les données du capteur au format JSON
@app.route('/data')
def get_data():
    data = rasp.lecture(Arduino)
    temp, hum, CO2 = data
    return jsonify(temperature=temp, humidity=hum, CO2=CO2)

# Route pour afficher la page avec les graphiques
@app.route('/graphiques')
def afficher_graphique():
    # Lire les données à partir du fichier CSV
    dates, temperatures, humidity, co2 = read_csv('donnees_capteur.csv')

    # Créer les graphiques
    fig, axs = plt.subplots(3, 1, figsize=(10, 8))

    # Graphique de la température
    axs[0].plot(dates, temperatures, color='red')
    axs[0].set_title('Température')
    axs[0].set_xlabel('Date')
    axs[0].set_ylabel('Température (°C)')

    # Graphique du CO2
    axs[1].plot(dates, co2, color='green')
    axs[1].set_title('CO2')
    axs[1].set_xlabel('Date')
    axs[1].set_ylabel('CO2 (ppm)')

    # Graphique de l'humidité
    axs[2].plot(dates, humidity, color='blue')
    axs[2].set_title('Humidité')
    axs[2].set_xlabel('Date')
    axs[2].set_ylabel('Humidité (%)')

    # Sauvegarder les graphiques en tant qu'images
    if not os.path.exists('static'):
        os.makedirs('static')
    plt.savefig('static/graphiques.png')
    plt.close()

    return render_template('graphique.html')

@app.route('/gestion')
def afficher_gestion():
    return render_template('gestion.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

# Fermer le port série à l'arrêt de l'application Flask
if Arduino.is_open:
    Arduino.close()


