import serial

def ouvrir_port(port, vitesse):
    return serial.Serial(port, vitesse)

def fermeture_port(port_serie):
    return port_serie.close()

def lecture(port_serie):
    data = port_serie.readline().decode("utf-8").rstrip()
    temp,CO2,hum = data.split(',')
    return float(temp), float(CO2), float(hum)
    

if __name__ == "__main__":
    Arduino = ouvrir_port('/dev/ttyACM0', 9600)
    while True:
        print(lecture(Arduino))
    fermeture_port(Arduino)
