import csv
import matplotlib.pyplot as plt

# Fonction pour lire les données à partir du fichier CSV
def read_csv(filename):
    dates = []
    temperatures = []
    co2 = []
    humidity = []

    with open(filename, 'r') as file:
        reader = csv.reader(file)
        next(reader)  # Skip header row
        for row in reader:
            if len(row) == 4:  # Vérifiez que la ligne contient le bon nombre de valeurs
                dates.append(row[0])
                temperatures.append(float(row[1]))
                co2.append(float(row[2]))
                humidity.append(float(row[3]))

    return dates, temperatures, co2, humidity

# Lire les données à partir du fichier CSV
dates, temperatures, co2, humidity = read_csv('donnees_capteur.csv')

# Créer un graphique
plt.figure(figsize=(10, 8))

# Sous-graphique pour la température
plt.subplot(3, 1, 1)
plt.plot(dates, temperatures, color='red')
plt.title('Température')
plt.xlabel('Date')
plt.ylabel('Température (°C)')

# Sous-graphique pour le CO2
plt.subplot(3, 1, 2)
plt.plot(dates, co2, color='green')
plt.title('CO2')
plt.xlabel('Date')
plt.ylabel('CO2 (ppm)')

# Sous-graphique pour l'humidité
plt.subplot(3, 1, 3)
plt.plot(dates, humidity, color='blue')
plt.title('Humidité')
plt.xlabel('Date')
plt.ylabel('Humidité (%)')

# Ajuster les espacements entre les sous-graphiques
plt.tight_layout()

# Afficher le graphique 
plt.show()
