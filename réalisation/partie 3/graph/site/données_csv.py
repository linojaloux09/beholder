import serial
import csv
import time

def ouvrir_port(port, vitesse):
    return serial.Serial(port, vitesse)

def fermeture_port(port_serie):
    return port_serie.close()

def lecture(port_serie):
    data = port_serie.readline().decode("utf-8").rstrip()
    temp, CO2, hum = data.split(',')
    return float(temp), float(CO2), float(hum)

def enregistrer_donnees_csv(data):
    fichier_csv = 'donnees_capteur.csv'
    with open(fichier_csv, 'a', newline='') as file:
        writer = csv.writer(file)
        if file.tell() == 0:
            writer.writerow(['Date', 'Température', 'CO2', 'Humidité'])
        writer.writerow([time.strftime('%Y-%m-%d %H:%M:%S'), *data])

if __name__ == "__main__":
    Arduino = ouvrir_port('/dev/ttyACM0', 9600)
    while True:
        data = lecture(Arduino)
        print(data)
        enregistrer_donnees_csv(data)
        time.sleep(1)  # Ajoutez une pause d'une seconde entre chaque lecture


