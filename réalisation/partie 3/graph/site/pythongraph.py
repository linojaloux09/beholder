import serial
import matplotlib.pyplot as plt
import csv
import time

# Fonction pour ouvrir le port série
def ouvrir_port(port, vitesse):
    return serial.Serial(port, vitesse)

# Fonction pour fermer le port série
def fermer_port(port_serie):
    port_serie.close()

# Fonction pour lire les données depuis le port série
def lecture_donnees(port_serie):
    data = port_serie.readline().decode("utf-8").rstrip()
    temp, CO2, hum = data.split(',')
    return float(temp), float(CO2), float(hum)

# Fonction pour enregistrer les données dans un fichier CSV
def enregistrer_donnees_csv(data):
    fichier_csv = 'donnees_capteur.csv'
    with open(fichier_csv, 'a', newline='') as file:
        writer = csv.writer(file)
        if file.tell() == 0:
            writer.writerow(['Date', 'Température', 'CO2', 'Humidité'])
        writer.writerow([time.strftime('%Y-%m-%d %H:%M:%S'), *data])

# Fonction pour afficher les graphiques
def afficher_graphiques():
    # Ouvrir le port série
    Arduino = ouvrir_port('/dev/ttyACM0', 9600)

    # Initialiser les listes de données
    temps = []
    temperatures = []
    co2_values = []
    humidites = []

    # Créer les sous-graphiques
    fig, (ax1pour actualiser les données toutes les minutes
    while True:
        # Lire les données depuis le port série
        temp, co2, hum = lecture_donnees(Arduino)

        # Enregistrer les données dans un fichier CSV
        enregistrer_donnees_csv([temp, co2, hum])

        # Ajouter les données aux listes
        temps.append(time.strftime('%H:%M:%S'))
        temperatures.append(temp)
        co2_values.append(co2)
        humidites.append(hum)

        # Afficher les données sur les graphiques
        ax1.plot(temps, temperatures, 'r-')
        ax2.plot(temps, co2_values, 'g-')
        ax3.plot(temps, humidites, 'b-')

        # Actualiser les graphiques
        plt.tight_layout()
        plt.draw()
        plt.pause(60)  # Actualisation toutes les 60 secondes

        # Enregistrer les graphiques comme images
        fig.savefig('static/temperature_plot.png')
        fig.savefig('static/co2_plot.png')
        fig.savefig('static/humidity_plot.png')

    # Fermer le port série
    fermer_port(Arduino)

if __name__ == "__main__":
    afficher_graphiques(), ax2, ax3) = plt.subplots(3, 1)

    # Titres des graphiques
    ax1.set_title('Température')
    ax2.set_title('CO2')
    ax3.set_title('Humidité')

    # Boucle pour actualiser les données toutes les minutes
    while True:
        # Lire les données depuis le port série
        temp, co2, hum = lecture_donnees(Arduino)

        # Enregistrer les données dans un fichier CSV
        enregistrer_donnees_csv([temp, co2, hum])

        # Ajouter les données aux listes
        temps.append(time.strftime('%H:%M:%S'))
        temperatures.append(temp)
        co2_values.append(co2)
        humidites.append(hum)

        # Afficher les données sur les graphiques
        ax1.plot(temps, temperatures, 'r-')
        ax2.plot(temps, co2_values, 'g-')
        ax3.plot(temps, humidites, 'b-')

        # Actualiser les graphiques
        plt.tight_layout()
        plt.draw()
        plt.pause(60)  # Actualisation toutes les 60 secondes

        # Enregistrer les graphiques comme images
        fig.savefig('static/temperature_plot.png')
        fig.savefig('static/co2_plot.png')
        fig.savefig('static/humidity_plot.png')

    # Fermer le port série
    fermer_port(Arduino)

if __name__ == "__main__":
    afficher_graphiques()
