from flask import Flask, render_template, jsonify
import rasp
import csv
import matplotlib.pyplot as plt
from datetime import datetime
import os

app = Flask(__name__)

# Fonction pour lire les données à partir du fichier CSV
def read_csv(filename):
    dates = []
    temperatures = []
    co2 = []
    humidity = []

    with open(filename, 'r') as file:
        reader = csv.reader(file)
        next(reader)  # Skip header row
        for row in reader:
            try:
                date = datetime.strptime(row[0], '%Y-%m-%d %H:%M:%S')
                temperature = float(row[1])
                co2_value = float(row[2])
                humidity_value = float(row[3])

                dates.append(date)
                temperatures.append(temperature)
                co2.append(co2_value)
                humidity.append(humidity_value)
            except (ValueError, IndexError):
                pass

    return dates, temperatures, co2, humidity

# Route pour afficher la page d'accueil
@app.route('/')
def index():
    return render_template("index.html")

# Route pour récupérer les données du capteur au format JSON
@app.route('/data')
def get_data():
    data = rasp.lecture(Arduino)
    temp, CO2, hum = data
    return jsonify(temperature=temp, CO2=CO2, humidity=hum)

# Route pour afficher la page avec les graphiques
@app.route('/graphiques')
def afficher_graphique():
    # Lire les données à partir du fichier CSV
    dates, temperatures, co2, humidity = read_csv('donnees_capteur.csv')

    # Créer les graphiques
    fig, axs = plt.subplots(3, 1, figsize=(10, 8))

    # Graphique de la température
    axs[0].plot(dates, temperatures, color='red')
    axs[0].set_title('Température')
    axs[0].set_xlabel('Date')
    axs[0].set_ylabel('Température (°C)')

    # Graphique du CO2
    axs[1].plot(dates, co2, color='green')
    axs[1].set_title('CO2')
    axs[1].set_xlabel('Date')
    axs[1].set_ylabel('CO2 (ppm)')

    # Graphique de l'humidité
    axs[2].plot(dates, humidity, color='blue')
    axs[2].set_title('Humidité')
    axs[2].set_xlabel('Date')
    axs[2].set_ylabel('Humidité (%)')

    # Sauvegarder les graphiques en tant qu'images
    if not os.path.exists('static'):
        os.makedirs('static')
    plt.savefig('static/graphiques.png')
    plt.close()

    return render_template('graphique.html')

if __name__ == '__main__':
    # Ouvrir le port série au démarrage de l'application Flask
    Arduino = rasp.ouvrir_port('/dev/ttyACM0', 9600)

    app.run(host='0.0.0.0', port=5000)

    # Fermer le port série à l'arrêt de l'application Flask
    if Arduino.is_open:
        Arduino.close() 


